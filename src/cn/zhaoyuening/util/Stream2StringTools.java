package cn.zhaoyuening.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class Stream2StringTools {

	public static String getString(InputStream in) throws IOException{
		byte[] buffer = new byte[1024];
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		int len = 0;
		//Reads some number of bytes from the input stream and stores them into the buffer array b.
		// The number of bytes actually read is returned as an integer.
		// This method blocks until input data is available, end of file is detected, or an exception is thrown.
		while((len=in.read(buffer))>0){
			//Writes len bytes from the specified byte array starting at offset off to this byte array output stream.
			baos.write(buffer, 0, len);
		}
		
		return baos.toString("utf-8");
	}
}
